;; init.el ~ acdw
;; I'm sure this will be badly written

;; package initiation
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile
  (require 'use-package))
(require 'use-package-ensure)
(setq use-package-always-ensure t)

;; custom file
(setq custom-file (concat user-emacs-directory "custom.el"))
;(if (file-exists-p custom-file) (load custom-file))

;; emacs-specific stuff

(setq user-full-name "Case Duckworth"
      user-mail-address "acdw@acdw.net")

(setq inhibit-startup-screen t)

(setq frame-title-format
      '((:eval (if (buffer-file-name)
		   (abbreviate-file-name (buffer-file-name))
		 "%b"))))

(setq backup-directory-alist `((".*" . ,(concat user-emacs-directory "temp")))
      delete-old-versions t
      kept-old-versions 100

      auto-save-file-name-transforms `((".*" ,(concat user-emacs-directory "temp") t)))

(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)

(line-number-mode)
;(global-display-line-numbers-mode)
(column-number-mode)
(size-indication-mode)

(tool-bar-mode -1)
(scroll-bar-mode -1)

(fset 'yes-or-no-p 'y-or-n-p)

(global-auto-revert-mode)
(if (version< emacs-version "25.0")
    (progn (require 'saveplace)
	   (setq-default save-place t))
  (save-place-mode 1))

(global-visual-line-mode)

(add-hook 'before-save-hook 'whitespace-cleanup t)

(set-face-attribute 'fixed-pitch nil :family "JetBrains Mono" :height 90)
(set-face-attribute 'default nil :family "JetBrains Mono" :height 90)

; https://endlessparentheses.com/fill-and-unfill-paragraphs-with-a-single-key.html
(defun endless/fill-or-unfill ()
  "Like `fill-paragraph`, but unfill if used twice."
  (interactive)
  (let ((fill-column
	 (if (eq last-command 'endless/fill-or-unfill)
	     (progn (setq this-command nil)
		    (point-max))
	   fill-column)))
    (call-interactively #'fill-paragraph)))

(global-set-key [remap fill-paragraph]
		#'endless/fill-or-unfill)

;; packages

(use-package savehist
  :config
  (savehist-mode t))

(use-package saveplace
  :config
  (progn
    (setq-default save-place t)
    (setq save-place-file (concat user-emacs-directory "saved-places"))))

(use-package elpher
  :custom
  (elpher-filter-ansi-from-text t)
  :custom-face
  (elpher-gemini-heading1 ((t (:inherit bold :underline t :slant italic))))
  (elpher-gemini-heading2 ((t (:inherit bold :slant italic))))
  (elpher-gemini-heading3 ((t (:inherit bold)))))

(use-package ansi-color)

(use-package modus-operandi-theme
  :init
  (setq modus-operandi-theme-slanted-constructs t
	modus-operandi-theme-variable-pitch-headings t)
  :config
  (load-theme 'modus-operandi t))

(use-package company
  :hook
  (after-init . global-company-mode))

(use-package gemini-mode)